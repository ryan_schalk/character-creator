package com.ryanschalk.charactercreator

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ryanschalk.charactercreator.createCharacter.model.RpgClass
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassName
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassesNetworkResponse
import com.ryanschalk.charactercreator.util.Resource
import java.lang.Exception
import java.math.BigDecimal

class TestRpgClassRepo: RpgClassRepo {
    private val _rpgClassesResource = MutableLiveData<Resource<RpgClassesNetworkResponse>>()
    override val rpgClassesResource: LiveData<Resource<RpgClassesNetworkResponse>> get() = _rpgClassesResource

    private val _purchaseMessage = MutableLiveData<String>().apply { value = "" }
    override val purchaseMessage: LiveData<String> get() = _purchaseMessage

    override fun retrieveRpgClasses() {
        val classes = arrayListOf(
            RpgClass(1, RpgClassName.WARRIOR, 8, 5, BigDecimal(5.00)),
            RpgClass(2, RpgClassName.BARD, 3, 5, BigDecimal.ZERO),
            RpgClass(3, RpgClassName.CAVALIER, 5, 5, BigDecimal.ZERO),
            RpgClass(4, RpgClassName.KNIGHT, 6, 10, BigDecimal.ZERO),
            RpgClass(5, RpgClassName.ARCHER, 8, 3, BigDecimal.ZERO),
            RpgClass(6, RpgClassName.THIEF, 3, 4, BigDecimal.ZERO),
            RpgClass(7, RpgClassName.TROUBADOUR, 3, 3, null)
        )

        _rpgClassesResource.value = Resource.success(
            RpgClassesNetworkResponse(
                classes
            )
        )
    }

    override fun retrieveRpgClassWithId(id: Int): RpgClass? {
        return when(id) {
            1 -> RpgClass(1, RpgClassName.WARRIOR, 8, 5, BigDecimal(5.00))
            2 -> RpgClass(2, RpgClassName.BARD, 3, 5, BigDecimal.ZERO)
            3 -> RpgClass(3, RpgClassName.CAVALIER, 5, 5, BigDecimal.ZERO)
            4 -> RpgClass(4, RpgClassName.KNIGHT, 6, 10, BigDecimal.ZERO)
            5 -> RpgClass(5, RpgClassName.ARCHER, 8, 3, BigDecimal.ZERO)
            6 -> RpgClass(6, RpgClassName.THIEF, 3, 4, BigDecimal.ZERO)
            7 -> RpgClass(7, RpgClassName.TROUBADOUR, 3, 3, null)
            else -> null
        }
    }

    override fun retrieveRpgClassesWithError() {
        _rpgClassesResource.value = Resource.error(Exception("You pressed the error button."), null)
    }

    override fun retrievePurchaseMessage() {
        _purchaseMessage.value = "Thank you for your purchase!"
    }
}