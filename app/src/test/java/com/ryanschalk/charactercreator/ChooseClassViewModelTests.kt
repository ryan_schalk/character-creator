package com.ryanschalk.charactercreator

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ryanschalk.charactercreator.createCharacter.chooseClass.ChooseClassViewModel
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class ChooseClassViewModelTests {
    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var sutViewModel: ChooseClassViewModel
    private lateinit var rpgClassRepo: RpgClassRepo

    @Before
    fun setUp() {
        rpgClassRepo = TestRpgClassRepo()

        sutViewModel = ChooseClassViewModel()
        sutViewModel.inject(ChooseClassViewModel.Dependencies(rpgClassRepo))
    }

    @Test
    fun test_purchase_dialog_shown_for_class_with_nonzero_cost() {
        sutViewModel.retrieveRpgClasses()

        sutViewModel.selectRpgClassAtPosition(0) // Warrior with cost of 5.00
        assertNotNull(sutViewModel.displayPurchaseDialog.getValueBlocking()!!.getContentIfNotHandled())
    }

    @Test
    fun test_purchase_dialog_not_shown_for_class_with_zero_cost() {
        sutViewModel.retrieveRpgClasses()

        sutViewModel.selectRpgClassAtPosition(1) // Bard with zero cost
        assertNull(sutViewModel.displayPurchaseDialog.getValueBlocking())
    }
}