package com.ryanschalk.charactercreator

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ryanschalk.charactercreator.createCharacter.assignStats.AssignStatsViewModel
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class AssignStatsViewModelTests {
    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var sutViewModel: AssignStatsViewModel
    private lateinit var rpgClassRepo: RpgClassRepo

    @Before
    fun setUp() {
        rpgClassRepo = TestRpgClassRepo()

        sutViewModel = AssignStatsViewModel()
        sutViewModel.inject(AssignStatsViewModel.Dependencies(rpgClassRepo, 2))
    }

    @Test
    fun test_create_disabled_by_default() {
        assertFalse(sutViewModel.createEnabled.getValueBlocking()!!)
    }

    @Test
    fun test_attack_and_defense_zero_by_default() {
        val actualAttackInfo = sutViewModel.attackInfo.getValueBlocking()!!
        val actualDefenseInfo = sutViewModel.defenseInfo.getValueBlocking()!!

        assertEquals("0", actualAttackInfo.attack)
        assertEquals("0", actualDefenseInfo.defense)
    }

    @Test
    fun test_attack_and_defense_incremented_successfully() {
        sutViewModel.plusAttack()
        assertEquals("1", sutViewModel.attackInfo.getValueBlocking()!!.attack)

        sutViewModel.plusDefense()
        sutViewModel.plusDefense()
        assertEquals("2", sutViewModel.defenseInfo.getValueBlocking()!!.defense)
    }

    @Test
    fun test_points_remaining_decrements_when_defense_increased() {
        assertEquals(10, sutViewModel.pointsRemaining.getValueBlocking()!!)
        sutViewModel.plusDefense()
        assertEquals(9, sutViewModel.pointsRemaining.getValueBlocking()!!)
    }

    @Test
    fun test_attack_cannot_exceed_maximum() {
        assertFalse(sutViewModel.attackInfo.getValueBlocking()!!.isMaxed)

        sutViewModel.plusAttack()
        sutViewModel.plusAttack()
        sutViewModel.plusAttack()
        assertTrue(sutViewModel.attackInfo.getValueBlocking()!!.isMaxed)

        sutViewModel.plusAttack() // 4th call to plusAttack()
        assertEquals("3", sutViewModel.attackInfo.getValueBlocking()!!.attack)
    }
}