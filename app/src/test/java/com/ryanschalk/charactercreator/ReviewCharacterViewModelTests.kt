package com.ryanschalk.charactercreator

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassIdAndStats
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import com.ryanschalk.charactercreator.createCharacter.review.ReviewCharacterFragment
import com.ryanschalk.charactercreator.createCharacter.review.ReviewCharacterViewModel
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class ReviewCharacterViewModelTests {
    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var sutViewModel: ReviewCharacterViewModel
    private lateinit var rpgClassRepo: RpgClassRepo

    @Before
    fun setUp() {
        rpgClassRepo = TestRpgClassRepo()

        sutViewModel = ReviewCharacterViewModel()
        sutViewModel.inject(ReviewCharacterViewModel.Dependencies(rpgClassRepo, RpgClassIdAndStats(6, 4, 1)))
    }

    @Test
    fun test_successful_conversion_from_injected_info_to_review_text_arguments() {
        assertEquals(
            ReviewCharacterFragment.ReviewTextArguments(R.string.warrior, "6", "4", "5"),
            sutViewModel.reviewTextArguments.getValueBlocking()!!
        )
    }
}