package com.ryanschalk.charactercreator

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/*
 * Reference article: [Testing Room and LiveData with Kotlin](http://kotlinblog.com/2017/11/17/testing-room-and-livedata/)
 * [Reference file](https://github.com/slomin/Testing-Room-LiveData/blob/master/app/src/androidTest/java/com/kotlinblog/roomlivedata/extensions/LiveDataExtensions.kt)
 */
@Throws(InterruptedException::class)
fun <T> LiveData<T>.getValueBlocking(): T? {
    var value: T? = null
    val latch = CountDownLatch(1)
    val innerObserver = Observer<T> {
        value = it
        latch.countDown()
    }
    observeForever(innerObserver)
    latch.await(3, TimeUnit.SECONDS)
    removeObserver(innerObserver)
    return value
}