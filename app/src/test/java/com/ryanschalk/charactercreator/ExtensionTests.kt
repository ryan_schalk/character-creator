package com.ryanschalk.charactercreator


import com.ryanschalk.charactercreator.createCharacter.model.RpgClass
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassName
import com.ryanschalk.charactercreator.util.correspondingDrawableResourceId
import com.ryanschalk.charactercreator.util.isFree
import com.ryanschalk.charactercreator.util.toStringResourceId
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.math.BigDecimal

class ExtensionTests {
    @Test
    fun test_null_big_decimal_is_considered_free() {
        val nullBigDecimal: BigDecimal? = null
        assertTrue(nullBigDecimal.isFree())
    }

    @Test
    fun test_rpg_class_name_converted_to_correct_string_id() {
        assertEquals(
            com.ryanschalk.charactercreator.createCharacter.model.RpgClassName.ARCHER.toStringResourceId(),
            R.string.archer
        )
        // ...
    }

    @Test
    fun test_rpg_class_name_converted_to_correct_color_id() {
        val archer = RpgClass(5, RpgClassName.ARCHER, 8, 3, BigDecimal.ZERO)
        assertEquals(archer.correspondingDrawableResourceId(), R.color.colorArcher)
        // ...
    }
}

