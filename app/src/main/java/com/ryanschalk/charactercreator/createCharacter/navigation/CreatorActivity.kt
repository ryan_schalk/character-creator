package com.ryanschalk.charactercreator.createCharacter.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.ryanschalk.charactercreator.*
import com.ryanschalk.charactercreator.createCharacter.assignStats.AssignStatsFragmentDirections
import com.ryanschalk.charactercreator.createCharacter.chooseClass.ChooseClassFragmentDirections
import com.ryanschalk.charactercreator.createCharacter.navigation.CharacterCreatorNavigationViewModel.NavigationCommand
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassNetworkRepo
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepoProvider
import com.ryanschalk.charactercreator.util.currentFragmentInNavHostFragment

class CreatorActivity: AppCompatActivity(), RpgClassRepoProvider {

    // Retrieve the repo from the ViewModel, creating a new Network repo if it doesn't already exist.
    override fun provideRepo(): RpgClassRepo {
        navViewModel.repo?.let { repo ->
            return repo
        } ?: run {
            val repo = RpgClassNetworkRepo()
            navViewModel.repo = repo
            return repo
        }
    }

    private val navViewModel: CharacterCreatorNavigationViewModel by lazy { ViewModelProviders.of(this).get(
        CharacterCreatorNavigationViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_creator)
        bindToViewModel()
    }

    override fun onBackPressed() {
        val backHandled = navViewModel.requestToNavigateBack()
        if (!backHandled) { super.onBackPressed() }
    }

    private fun bindToViewModel() {
        navViewModel.navigationCommand.observe(this, Observer {
            it?.let { event ->
                val command = event.getContentIfNotHandled() ?: return@Observer

                val currentFragment = currentFragmentInNavHostFragment(R.id.creator_nav_host_fragment) ?: return@Observer
                val navController = NavHostFragment.findNavController(currentFragment)

                when(command) {
                    is NavigationCommand.ChooseClassToAssignStats -> {
                        val directions =
                            ChooseClassFragmentDirections.actionChooseClassFragmentToAssignStatsFragment()
                        directions.setRPGCLASSID(command.rpgClassId)
                        navController.navigate(directions)
                    }
                    is NavigationCommand.AssignStatsToReview -> {
                        val directions =
                            AssignStatsFragmentDirections.actionAssignStatsFragmentToReviewCharacterFragment()
                        directions.setATTACK(command.rpgClassIdAndStats.attack)
                        directions.setDEFENSE(command.rpgClassIdAndStats.defense)
                        directions.setRPGCLASSID(command.rpgClassIdAndStats.rpgClassId)
                        navController.navigate(directions)
                    }
                    is NavigationCommand.Finish -> finish()
                }
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        navViewModel.onSaveInstanceState(outState)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        navViewModel.onRestoreInstanceState(savedInstanceState)
        super.onRestoreInstanceState(savedInstanceState)
    }
}
