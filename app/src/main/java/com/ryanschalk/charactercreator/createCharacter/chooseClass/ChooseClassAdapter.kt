package com.ryanschalk.charactercreator.createCharacter.chooseClass

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.ryanschalk.charactercreator.R
import com.ryanschalk.charactercreator.util.inflate
import kotlinx.android.synthetic.main.class_recycler_view_item.view.*

class ChooseClassAdapter(private val onClickItem: (Int) -> Unit): RecyclerView.Adapter<ChooseClassAdapter.ClassViewHolder>()  {

    sealed class RpgClassListItemInfo {
        data class RpgClassListPaidItemInfo(val nameResourceId: Int, val colorResourceId: Int, val cost: String) :
            RpgClassListItemInfo()
        data class RpgClassListFreeItemInfo(val nameResourceId: Int, val colorResourceId: Int) :
            RpgClassListItemInfo()
    }

    var choices: List<RpgClassListItemInfo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassViewHolder {
        return ClassViewHolder(
            parent.inflate(
                R.layout.class_recycler_view_item
            ), onClickItem
        )
    }

    override fun getItemCount(): Int {
        return choices.size
    }

    override fun onBindViewHolder(holder: ClassViewHolder, position: Int) {
        holder.bindChoice(choices[position])
    }

    class ClassViewHolder(private val view: View, private val onClickItem: (Int) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bindChoice(itemInfo: RpgClassListItemInfo) {
            when(itemInfo) {
                is RpgClassListItemInfo.RpgClassListPaidItemInfo -> {
                    view.classNameTextView.text = String.format(view.context.getString(R.string.rpg_class_list_item_text),
                                view.context.getString(itemInfo.nameResourceId),
                                itemInfo.cost)

                    view.colorImageView.setImageResource(itemInfo.colorResourceId)
                }
                is RpgClassListItemInfo.RpgClassListFreeItemInfo -> {
                    view.classNameTextView.text = String.format(view.context.getString(R.string.rpg_class_list_item_text_free),
                        view.context.getString(itemInfo.nameResourceId))

                    view.colorImageView.setImageResource(itemInfo.colorResourceId)
                }
            }

            view.setOnClickListener {
                onClickItem(adapterPosition)
            }
        }
    }
}