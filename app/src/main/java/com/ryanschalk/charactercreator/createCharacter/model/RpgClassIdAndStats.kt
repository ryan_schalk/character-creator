package com.ryanschalk.charactercreator.createCharacter.model

data class RpgClassIdAndStats(val attack: Int, val defense: Int, val rpgClassId: Int)