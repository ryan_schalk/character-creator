package com.ryanschalk.charactercreator.createCharacter.chooseClass

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ryanschalk.charactercreator.createCharacter.navigation.CharacterCreatorNavigationViewModel
import com.ryanschalk.charactercreator.util.DialogUtil
import com.ryanschalk.charactercreator.R
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepoProvider
import kotlinx.android.synthetic.main.fragment_choose_class.*


class ChooseClassFragment : Fragment() {
    private val viewModel: ChooseClassViewModel by lazy { ViewModelProviders.of(this).get(
        ChooseClassViewModel::class.java) }
    private val navViewModel: CharacterCreatorNavigationViewModel by lazy { ViewModelProviders.of(requireActivity()).get(
        CharacterCreatorNavigationViewModel::class.java) }

    private val adapter = ChooseClassAdapter { position ->
        viewModel.selectRpgClassAtPosition(position)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_choose_class, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.inject(ChooseClassViewModel.Dependencies((activity as RpgClassRepoProvider).provideRepo()))

        bindToViewModel()


        classRecyclerView.layoutManager = LinearLayoutManager(context)
        classRecyclerView.adapter = adapter
    }

    private fun bindToViewModel() {
        getClassesButton.setOnClickListener { viewModel.retrieveRpgClasses() }
        getClassesWithErrorButton.setOnClickListener { viewModel.retrieveRpgClassesWithError() }

        viewModel.completeWithRpgClassId.observe(viewLifecycleOwner, Observer {
            it?.let { event ->
                val id = event.getContentIfNotHandled() ?: return@Observer
                navViewModel.chooseRpgClass(id)
            }
        })

        viewModel.rpgClassInfoList.observe(viewLifecycleOwner, Observer {
            it?.let { list ->
                adapter.choices = list
            }
        })

        viewModel.displayErrorMessage.observe(viewLifecycleOwner, Observer {
            it?.let { event ->
                val message = event.getContentIfNotHandled() ?: return@Observer
                DialogUtil.displayErrorDialog(context, message)
            }
        })

        viewModel.displayPurchaseDialog.observe(viewLifecycleOwner, Observer {
            it?.let { event ->
                event.getContentIfNotHandled()?.let { _ ->
                    confirmPurchase()
                }
            }
        })

        viewModel.showLoadingIndicator.observe(viewLifecycleOwner, Observer {
            it?.let { show ->
                progressBar.visibility = if (show) View.VISIBLE else View.INVISIBLE
            }
        })
    }

    private fun confirmPurchase() {
        DialogUtil.displayConfirmationDialog(
            context,
            getString(R.string.confirm_purchase),
            DialogInterface.OnClickListener { _, _ ->
                viewModel.confirmPurchase()
            })
    }
}
