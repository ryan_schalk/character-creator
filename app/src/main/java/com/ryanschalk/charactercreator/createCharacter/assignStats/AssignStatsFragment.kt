package com.ryanschalk.charactercreator.createCharacter.assignStats


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ryanschalk.charactercreator.createCharacter.navigation.CharacterCreatorNavigationViewModel
import com.ryanschalk.charactercreator.R
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepoProvider
import kotlinx.android.synthetic.main.fragment_assign_stats.*

class AssignStatsFragment : Fragment() {

    data class AttackInfo(val attack: String, val isMaxed: Boolean)
    data class DefenseInfo(val defense: String, val isMaxed: Boolean)

    private val viewModel: AssignStatsViewModel by lazy { ViewModelProviders.of(this).get(
        AssignStatsViewModel::class.java) }
    private val navViewModel: CharacterCreatorNavigationViewModel by lazy { ViewModelProviders.of(requireActivity()).get(
        CharacterCreatorNavigationViewModel::class.java) }

    override fun onCreateView (inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_assign_stats, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val id = AssignStatsFragmentArgs.fromBundle(arguments).rpgclassid
        viewModel.inject(
            AssignStatsViewModel.Dependencies(
                (activity as RpgClassRepoProvider).provideRepo(),
                id
            )
        )

        viewModel.onRestoreInstanceState(savedInstanceState)
        bindToViewModel()
    }

    private fun bindToViewModel() {
        attackPlusButton.setOnClickListener { viewModel.plusAttack() }
        defensePlusButton.setOnClickListener { viewModel.plusDefense() }
        attackMinusButton.setOnClickListener { viewModel.minusAttack() }
        defenseMinusButton.setOnClickListener { viewModel.minusDefense() }
        createButton.setOnClickListener { viewModel.create() }

        viewModel.completeWithRpgClassIdAndStats.observe(viewLifecycleOwner, Observer {
            it?.let { event ->
                val info = event.getContentIfNotHandled() ?: return@Observer
                navViewModel.assignStatsToReview(info)
            }
        })

        viewModel.createEnabled.observe(viewLifecycleOwner, Observer {
            it?.let { enabled ->
                createButton.isEnabled = enabled
            }
        })

        viewModel.rpgClassNameResourceId.observe(viewLifecycleOwner, Observer {
            it?.let { nameResourceId ->
                context?.let { unwrappedContext ->
                    instructionalMessageTextView.text = String.format(unwrappedContext.getString(R.string.assign_stats_for_your_character),
                        unwrappedContext.getString(nameResourceId))
                }
            }
        })

        viewModel.attackInfo.observe(viewLifecycleOwner, Observer {
            it?.let { attackInfo ->
                val unwrappedContext = context ?: return@Observer
                attackTextView.text = if (attackInfo.isMaxed) {
                    String.format(unwrappedContext.getString(R.string.attack_info_maxed), attackInfo.attack)
                } else {
                    String.format(unwrappedContext.getString(R.string.attack_info), attackInfo.attack)
                }
            }
        })

        viewModel.defenseInfo.observe(viewLifecycleOwner, Observer {
            it?.let { defenseInfo ->
                val unwrappedContext = context ?: return@Observer
                defenseTextView.text = if (defenseInfo.isMaxed) {
                    String.format(unwrappedContext.getString(R.string.defense_info_maxed), defenseInfo.defense)
                } else {
                    String.format(unwrappedContext.getString(R.string.defense_info), defenseInfo.defense)
                }
            }
        })

        viewModel.pointsRemaining.observe(viewLifecycleOwner, Observer {
            it?.let { points ->
                context?.let { context ->
                    remainingPointsTextView.text = String.format(context.getString(R.string.points_remaining), points)
                }
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.onSaveInstanceState(outState)
    }
}
