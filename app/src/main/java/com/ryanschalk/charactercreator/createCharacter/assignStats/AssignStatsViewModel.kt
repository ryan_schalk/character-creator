package com.ryanschalk.charactercreator.createCharacter.assignStats

import android.os.Bundle
import androidx.lifecycle.*
import com.ryanschalk.charactercreator.createCharacter.model.RpgClass
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassIdAndStats
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import com.ryanschalk.charactercreator.util.*

class AssignStatsViewModel: ViewModel(), SavedInstanceStateAware {

    private lateinit var repo: RpgClassRepo
    data class Dependencies(val rpgClassRepo: RpgClassRepo, val rpgClassId: Int)
    fun inject(dependencies: Dependencies) {
        if (!this::repo.isInitialized) {
            repo = dependencies.rpgClassRepo
            _chosenClass.value = repo.retrieveRpgClassWithId(dependencies.rpgClassId)
        }
    }

    companion object { private const val TOTAL_USABLE_POINTS = 10 }

    private val _chosenClass = MutableLiveData<RpgClass>()
    private val _attack = MutableLiveData<Int>().apply { value = 0 }
    private val _defense = MutableLiveData<Int>().apply { value = 0 }

    private fun maxAttack(): Int? { return _chosenClass.value?.maxAttack }
    private fun maxDefense(): Int? { return _chosenClass.value?.maxDefense }

    private fun attackIsMaxed(): Boolean {
        val currentAttack = _attack.value ?: return false
        val max = maxAttack() ?: return false
        return currentAttack >= max
    }

    private fun defenseIsMaxed(): Boolean {
        val currentDefense = _defense.value ?: return false
        val max = maxDefense() ?: return false
        return currentDefense >= max
    }

    private fun verifyNoMorePointsCanBeAllocated(): Boolean {
        if (attackIsMaxed() && defenseIsMaxed()) { return true }
        _pointsRemaining.value?.let { return it == 0 }

        return false
    }

    // region Output
    private val _pointsRemaining = MutableLiveData<Int>().apply { value = TOTAL_USABLE_POINTS }
    val pointsRemaining: LiveData<Int> get() = _pointsRemaining

    private val _completeWithRpgClassIdAndStats = MutableLiveData<Event<RpgClassIdAndStats>>()
    val completeWithRpgClassIdAndStats: LiveData<Event<RpgClassIdAndStats>> get() = _completeWithRpgClassIdAndStats

    val attackInfo: LiveData<AssignStatsFragment.AttackInfo> = Transformations.map(_attack) {
        AssignStatsFragment.AttackInfo(
            it.toString(),
            attackIsMaxed()
        )
    }

    val defenseInfo: LiveData<AssignStatsFragment.DefenseInfo> = Transformations.map(_defense) {
        AssignStatsFragment.DefenseInfo(
            it.toString(),
            defenseIsMaxed()
        )
    }

    val createEnabled: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        this.addSource(_attack) { value = verifyNoMorePointsCanBeAllocated() }
        this.addSource(_defense) { value = verifyNoMorePointsCanBeAllocated() }
        this.addSource(_chosenClass) { value = verifyNoMorePointsCanBeAllocated() }
    }

    val rpgClassNameResourceId: LiveData<Int> = Transformations.map(_chosenClass) {
        it.name.toStringResourceId()
    }
    // endregion Output

    // region Input
    fun plusAttack() {
        val remaining = _pointsRemaining.value ?: return
        if (attackIsMaxed() || remaining == 0) { return }
        _pointsRemaining.decrement()
        _attack.increment()
    }

    fun plusDefense() {
        val remaining = _pointsRemaining.value ?: return
        if (defenseIsMaxed() || remaining == 0) { return }
        _pointsRemaining.decrement()
        _defense.increment()
    }

    fun minusAttack() {
        val currentAttack = _attack.value ?: return
        if (currentAttack <= 0) { return }
        _pointsRemaining.increment()
        _attack.decrement()
    }

    fun minusDefense() {
        val currentDefense = _defense.value ?: return
        if (currentDefense <= 0) { return }
        _pointsRemaining.increment()
        _defense.decrement()
    }

    fun create() {
        val attack = _attack.value ?: return
        val defense = _defense.value ?: return
        val chosenClass = _chosenClass.value ?: return

        _completeWithRpgClassIdAndStats.value = Event(
            RpgClassIdAndStats(
                attack,
                defense,
                chosenClass.id
            )
        )
    }
    // endregion Input

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.let { bundle ->
            _attack.value?.let { bundle.putInt("attack", it) }
            _defense.value?.let { bundle.putInt("defense", it) }
            _pointsRemaining.value?.let { bundle.putInt("pointsRemaining", it) }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.let { bundle ->
            _attack.value = bundle.getInt("attack")
            _defense.value = bundle.getInt("defense")
            _pointsRemaining.value = bundle.getInt("pointsRemaining")
        }
    }
}