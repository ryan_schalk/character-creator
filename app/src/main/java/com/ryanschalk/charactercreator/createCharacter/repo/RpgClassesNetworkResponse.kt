package com.ryanschalk.charactercreator.createCharacter.repo

import com.ryanschalk.charactercreator.createCharacter.model.RpgClass

data class RpgClassesNetworkResponse(val rpgClasses: List<RpgClass>, val status: String?) {
    constructor(rpgClasses: List<RpgClass>) : this(rpgClasses, null)
}