package com.ryanschalk.charactercreator.createCharacter.repo

interface RpgClassRepoProvider {
    fun provideRepo(): RpgClassRepo
}