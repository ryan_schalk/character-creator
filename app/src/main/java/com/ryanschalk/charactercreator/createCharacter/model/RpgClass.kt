package com.ryanschalk.charactercreator.createCharacter.model

import java.io.Serializable
import java.math.BigDecimal

enum class RpgClassName {
    ARCHER,
    BARD,
    CAVALIER,
    KNIGHT,
    THIEF,
    TROUBADOUR,
    WARRIOR
}

data class RpgClass(val id: Int, val name: RpgClassName, val maxAttack: Int, val maxDefense: Int, val cost: BigDecimal?): Serializable