package com.ryanschalk.charactercreator.createCharacter.review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.ryanschalk.charactercreator.createCharacter.model.RpgClass
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassIdAndStats
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import com.ryanschalk.charactercreator.util.correspondingDrawableResourceId
import com.ryanschalk.charactercreator.util.toStringResourceId
import java.math.BigDecimal

class ReviewCharacterViewModel: ViewModel() {
    private lateinit var repo: RpgClassRepo

    data class Dependencies(val rpgClassRepo: RpgClassRepo, val rpgClassIdAndStats: RpgClassIdAndStats)
    fun inject(dependencies: Dependencies) {

        if (!this::repo.isInitialized) {
            repo = dependencies.rpgClassRepo
            repo.retrieveRpgClassWithId(dependencies.rpgClassIdAndStats.rpgClassId)?.let { rpgClass ->
                _chosenClass.value = rpgClass

                val attack = dependencies.rpgClassIdAndStats.attack
                val defense = dependencies.rpgClassIdAndStats.defense

                _reviewTextArguments.value = calculateReviewTextArguments(rpgClass, attack, defense)
            }
        }
    }

    private val _chosenClass = MutableLiveData<RpgClass>()

    // region Output
    private val _reviewTextArguments = MutableLiveData<ReviewCharacterFragment.ReviewTextArguments>()
    val reviewTextArguments: LiveData<ReviewCharacterFragment.ReviewTextArguments> get() = _reviewTextArguments

    val purchaseMessage: LiveData<String> by lazy { Transformations.map(repo.purchaseMessage) { message ->
        val classIsPaidClass = (_chosenClass.value?.cost ?: BigDecimal.ZERO) > BigDecimal.ZERO
        if (classIsPaidClass) message else ""
    }}

    val reviewHeaderColorId: LiveData<Int> = Transformations.map(_chosenClass) {
        it.correspondingDrawableResourceId()
    }
    // endregion Output

    private fun calculateReviewTextArguments(chosenClass: RpgClass, attack: Int, defense: Int): ReviewCharacterFragment.ReviewTextArguments {
        return ReviewCharacterFragment.ReviewTextArguments(
            chosenClass.name.toStringResourceId(),
            attack.toString(),
            defense.toString(),
            chosenClass.cost.toString()
        )
    }
}