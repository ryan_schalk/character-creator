package com.ryanschalk.charactercreator.createCharacter.navigation

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ryanschalk.charactercreator.util.Event
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassIdAndStats
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import com.ryanschalk.charactercreator.util.SavedInstanceStateAware

class CharacterCreatorNavigationViewModel: ViewModel(), SavedInstanceStateAware {

    // The repo shared by all Fragments in the flow.
    // The Activity is the provider, but the repo is stored here so that it is not deallocated upon configuration change.
    // This approach is an alternative to using something like Dagger.
    var repo: RpgClassRepo? = null

    private enum class NavigationState { CHOOSE_CLASS, ASSIGN_STATS, REVIEW }

    sealed class NavigationCommand {
        data class ChooseClassToAssignStats(val rpgClassId: Int): NavigationCommand()
        data class AssignStatsToReview(val rpgClassIdAndStats: RpgClassIdAndStats): NavigationCommand()
        object Finish: NavigationCommand()
    }

    private val _navigationState = MutableLiveData<NavigationState>().apply { value = NavigationState.CHOOSE_CLASS }

    private val _navigationCommand = MutableLiveData<Event<NavigationCommand>>()
    val navigationCommand: LiveData<Event<NavigationCommand>> get() = _navigationCommand

    // region Commands: Completed Steps
    fun chooseRpgClass(rpgClassId: Int) {
        _navigationState.value =
                NavigationState.ASSIGN_STATS
        _navigationCommand.value = Event(
            NavigationCommand.ChooseClassToAssignStats(
                rpgClassId
            )
        )
    }

    fun assignStatsToReview(rpgClassIdAndStats: RpgClassIdAndStats) {
        _navigationState.value =
                NavigationState.REVIEW
        _navigationCommand.value = Event(
            NavigationCommand.AssignStatsToReview(
                rpgClassIdAndStats
            )
        )
    }

    fun done() {
        _navigationCommand.value =
                Event(NavigationCommand.Finish)
    }
    // endregion Commands: Completed Steps

    // Returns true if handled and false otherwise.
    fun requestToNavigateBack(): Boolean {
        return when(_navigationState.value) {
            NavigationState.ASSIGN_STATS -> {
                _navigationState.value =
                        NavigationState.CHOOSE_CLASS
                false
            }
            NavigationState.REVIEW -> {
                _navigationCommand.value =
                        Event(NavigationCommand.Finish)
                true
            }
            else -> false
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.let { bundle ->
            _navigationState.value?.let { bundle.putSerializable("navigationState", it) }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.let { bundle ->
            (bundle.getSerializable("navigationState") as? NavigationState)?.let { _navigationState.value = it }
        }
    }
}