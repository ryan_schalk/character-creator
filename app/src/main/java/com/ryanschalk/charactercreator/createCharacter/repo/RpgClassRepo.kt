package com.ryanschalk.charactercreator.createCharacter.repo

import androidx.lifecycle.LiveData
import com.ryanschalk.charactercreator.util.Resource
import com.ryanschalk.charactercreator.createCharacter.model.RpgClass

interface RpgClassRepo {
    val rpgClassesResource: LiveData<Resource<RpgClassesNetworkResponse>>
    val purchaseMessage: LiveData<String>

    fun retrieveRpgClasses()
    fun retrieveRpgClassesWithError()
    fun retrievePurchaseMessage()
    fun retrieveRpgClassWithId(id: Int): RpgClass?
}