package com.ryanschalk.charactercreator.createCharacter.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ryanschalk.charactercreator.util.Resource
import com.ryanschalk.charactercreator.createCharacter.model.RpgClass
import java.lang.Exception
import java.math.BigDecimal
import kotlin.concurrent.thread
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassName.*

class RpgClassNetworkRepo: RpgClassRepo {
    private val _rpgClassesResource = MutableLiveData<Resource<RpgClassesNetworkResponse>>()
    override val rpgClassesResource: LiveData<Resource<RpgClassesNetworkResponse>> get() = _rpgClassesResource

    private val _purchaseMessage = MutableLiveData<String>()
    override val purchaseMessage: LiveData<String> get() = _purchaseMessage

    companion object {
        const val ARTIFICIAL_DELAY: Long = 500
    }

    override fun retrieveRpgClassWithId(id: Int): RpgClass? {

        return when(id) {
            1 -> RpgClass(1, WARRIOR, 8, 5, BigDecimal(5.00))
            2 -> RpgClass(2, BARD, 3, 5, BigDecimal.ZERO)
            3 -> RpgClass(3, CAVALIER, 5, 5, BigDecimal.ZERO)
            4 -> RpgClass(4, KNIGHT, 6, 10, BigDecimal.ZERO)
            5 -> RpgClass(5, ARCHER, 8, 3, BigDecimal.ZERO)
            6 -> RpgClass(6, THIEF, 3, 4, BigDecimal.ZERO)
            7 -> RpgClass(7, TROUBADOUR, 3, 3, null)
            else -> null
        }
    }

    override fun retrieveRpgClasses() {
        _rpgClassesResource.value = Resource.loading(null)

        thread {
            Thread.sleep(ARTIFICIAL_DELAY)

            val classes = arrayListOf(
                RpgClass(1, WARRIOR, 8, 5, BigDecimal(5.00)),
                RpgClass(2, BARD, 3, 5, BigDecimal.ZERO),
                RpgClass(3, CAVALIER, 5, 5, BigDecimal.ZERO),
                RpgClass(4, KNIGHT, 6, 10, BigDecimal.ZERO),
                RpgClass(5, ARCHER, 8, 3, BigDecimal.ZERO),
                RpgClass(6, THIEF, 3, 4, BigDecimal.ZERO),
                RpgClass(7, TROUBADOUR, 3, 3, null)
            )

            _rpgClassesResource.postValue(
                Resource.success(
                    RpgClassesNetworkResponse(
                        classes
                    )
                )
            )
        }
    }

    override fun retrieveRpgClassesWithError() {
        _rpgClassesResource.value = Resource.loading(null)

        thread {
            Thread.sleep(ARTIFICIAL_DELAY)
            _rpgClassesResource.postValue(
                Resource.error(
                    Exception("You pressed the error button."),
                    null
                )
            )
        }
    }

    override fun retrievePurchaseMessage() {
        thread {
            Thread.sleep(10000)
            _purchaseMessage.postValue("Thank you for your purchase!")
        }
    }
}