package com.ryanschalk.charactercreator.createCharacter.review


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ryanschalk.charactercreator.*
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassIdAndStats
import com.ryanschalk.charactercreator.createCharacter.navigation.CharacterCreatorNavigationViewModel
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepoProvider
import kotlinx.android.synthetic.main.fragment_review_character.*
import java.io.Serializable


class ReviewCharacterFragment : Fragment() {

    data class ReviewTextArguments(val classNameResourceId: Int, val attack: String, val defense: String, val cost: String): Serializable

    private val viewModel: ReviewCharacterViewModel by lazy { ViewModelProviders.of(this).get(
        ReviewCharacterViewModel::class.java) }
    private val navViewModel: CharacterCreatorNavigationViewModel by lazy { ViewModelProviders.of(requireActivity()).get(
        CharacterCreatorNavigationViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_review_character, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val id = ReviewCharacterFragmentArgs.fromBundle(arguments).rpgclassid
        val attack = ReviewCharacterFragmentArgs.fromBundle(arguments).attack
        val defense = ReviewCharacterFragmentArgs.fromBundle(arguments).defense
        val info = RpgClassIdAndStats(attack, defense, id)
        viewModel.inject(
            ReviewCharacterViewModel.Dependencies(
                (activity as RpgClassRepoProvider).provideRepo(),
                info
            )
        )

        bindToViewModel()
        doneButton.setOnClickListener { navViewModel.done() }
    }

    private fun bindToViewModel() {
        viewModel.reviewTextArguments.observe(viewLifecycleOwner, Observer {
            it?.let { reviewTextArguments ->
                context?.let { unwrappedContext ->
                    reviewTextView.text = String.format(unwrappedContext.getString(R.string.review_text),
                        unwrappedContext.getString(reviewTextArguments.classNameResourceId),
                        reviewTextArguments.attack, reviewTextArguments.defense,
                        reviewTextArguments.cost
                    )
                }
            }
        })

        viewModel.reviewHeaderColorId.observe(viewLifecycleOwner, Observer {
            it?.let { id ->
                rpgClassImageView.setImageResource(id)
            }
        })

        viewModel.purchaseMessage.observe(viewLifecycleOwner, Observer {
            it?.let { purchaseMessage ->
                purchaseMessageTextView.text = purchaseMessage
            } ?: run {
                purchaseMessageTextView.text = ""
            }
        })
    }
}
