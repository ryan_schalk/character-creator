package com.ryanschalk.charactercreator.createCharacter.chooseClass

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.ryanschalk.charactercreator.createCharacter.model.RpgClass
import com.ryanschalk.charactercreator.createCharacter.repo.RpgClassRepo
import com.ryanschalk.charactercreator.util.*

class ChooseClassViewModel: ViewModel() {
    private lateinit var repo: RpgClassRepo

    data class Dependencies(val rpgClassRepo: RpgClassRepo)
    fun inject(dependencies: Dependencies) {

        // This check is required. If the repo is reset after rotation, Transformations on repo info will not work.
        if (!this::repo.isInitialized) {
            repo = dependencies.rpgClassRepo
        }
    }

    private var chosenClass: RpgClass? = null

    private val _completeWithRpgClassId = MutableLiveData<Event<Int>>()
    val completeWithRpgClassId: LiveData<Event<Int>> get () = _completeWithRpgClassId

    private val _displayPurchaseDialog = MutableLiveData<Event<Unit>>()
    val displayPurchaseDialog: LiveData<Event<Unit>> get() = _displayPurchaseDialog

    private fun completeThisStep() {
        val chosenClass = this.chosenClass ?: return

        // If the class costs money, retrieve a thank-you message to be displayed to the user after the purchase actually goes through.
        chosenClass.cost?.let {
            if (!it.isFree()) { repo.retrievePurchaseMessage() }
        }

        _completeWithRpgClassId.value = Event(chosenClass.id)
    }

    // region Output
    val showLoadingIndicator: LiveData<Boolean>
        get() = Transformations.map(repo.rpgClassesResource) { it.status == ResourceStatus.LOADING }

    // When RPG classes are received, map them to information that can be displayed by the View.
    val rpgClassInfoList: LiveData<List<ChooseClassAdapter.RpgClassListItemInfo>?> by lazy { Transformations.map(repo.rpgClassesResource) {
        fun mapResourceToViewConsumableList(): List<ChooseClassAdapter.RpgClassListItemInfo>? {
            if (it.status == ResourceStatus.SUCCESS) {
                it.data?.let { response ->
                    return response.rpgClasses.map { rpgClass ->
                        val nameResourceId = rpgClass.name.toStringResourceId()
                        val colorResourceId = rpgClass.correspondingDrawableResourceId()

                        if (rpgClass.cost.isFree()) {
                            ChooseClassAdapter.RpgClassListItemInfo.RpgClassListFreeItemInfo(
                                nameResourceId,
                                colorResourceId
                            )
                        } else {
                            ChooseClassAdapter.RpgClassListItemInfo.RpgClassListPaidItemInfo(
                                nameResourceId,
                                colorResourceId,
                                rpgClass.cost.toString()
                            )
                        }
                    }
                }
            }

            return null
        }

        mapResourceToViewConsumableList()
    }}

    val displayErrorMessage: LiveData<Event<String>?> by lazy { Transformations.map(repo.rpgClassesResource) {
        fun mapResourceToErrorMessage(): Event<String>? {
            if (it.status == ResourceStatus.ERROR) {
                it.exception?.message?.let { unwrappedMessage ->
                    return Event(unwrappedMessage)
                }
            }

            return null
        }

        mapResourceToErrorMessage()
    }}
    // endregion Output

    // region Input
    fun retrieveRpgClasses() { repo.retrieveRpgClasses() }
    fun retrieveRpgClassesWithError() { repo.retrieveRpgClassesWithError() }

    fun selectRpgClassAtPosition(position: Int) {
        // Remember the selected class.
        val rpgClasses = repo.rpgClassesResource.value?.data?.rpgClasses ?: return
        val chosenClass = rpgClasses[position]
        this.chosenClass = chosenClass

        if (chosenClass.cost != null && !chosenClass.cost.isFree()) {
            _displayPurchaseDialog.value = Event(Unit)
        } else {
            completeThisStep()
        }
    }

    fun confirmPurchase() {
        completeThisStep()
    }
    // endregion Input
}