package com.ryanschalk.charactercreator.util

// Reference article: [LiveData with...Events](https://medium.com/androiddevelopers/livedata-with-snackbar-navigation-and-other-events-the-singleliveevent-case-ac2622673150)
open class Event<out T>(private val content: T) {
    private var hasBeenHandled = false

    @Synchronized
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }
}