package com.ryanschalk.charactercreator.util

import androidx.lifecycle.MutableLiveData
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ryanschalk.charactercreator.R
import com.ryanschalk.charactercreator.createCharacter.model.RpgClass
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassName
import java.lang.Exception
import java.math.BigDecimal
import com.ryanschalk.charactercreator.createCharacter.model.RpgClassName.*

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun MutableLiveData<Int>.increment() {
    value = (value ?: 0) + 1
}

fun MutableLiveData<Int>.decrement() {
    value = (value ?: 0) - 1
}

fun AppCompatActivity.currentFragmentInNavHostFragment(navHostFragmentId: Int): Fragment? {
    return try {
        supportFragmentManager.findFragmentById(navHostFragmentId)?.childFragmentManager?.fragments?.last()
    } catch (e: Exception) {
        null
    }
}

// If the BigDecimal is null, we should assume there is no cost.
fun BigDecimal?.isFree(): Boolean {
    return this == BigDecimal.ZERO || this == null
}

fun RpgClassName?.toStringResourceId(): Int {
    return if (this == null) {
        R.string.character
    } else { when(this) {
        ARCHER -> R.string.archer
        BARD -> R.string.bard
        CAVALIER -> R.string.cavalier
        KNIGHT -> R.string.knight
        THIEF -> R.string.thief
        TROUBADOUR -> R.string.troubadour
        WARRIOR -> R.string.warrior
    }}
}

fun RpgClass.correspondingDrawableResourceId(): Int {
    return when(this.name) {
        BARD -> R.color.colorBard
        WARRIOR -> R.color.colorWarrior
        CAVALIER -> R.color.colorCavalier
        KNIGHT -> R.color.colorKnight
        ARCHER -> R.color.colorArcher
        THIEF -> R.color.colorThief
        TROUBADOUR -> R.color.colorTroubadour
    }
}