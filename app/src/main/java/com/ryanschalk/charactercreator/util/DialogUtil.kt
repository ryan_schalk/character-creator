package com.ryanschalk.charactercreator.util

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

class DialogUtil {
    companion object {
        fun displayErrorDialog(context: Context?, message: String?) {
            AlertDialog.Builder(context ?: return)
                .setTitle("ERROR!!!")
                .setMessage(message)
                .setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
                .create()
                .show()
        }

        fun displayConfirmationDialog(context: Context?, message: String?, positiveButtonListener: DialogInterface.OnClickListener) {
            AlertDialog.Builder(context ?: return)
                .setTitle("Please Confirm")
                .setMessage(message)
                .setNegativeButton("No") { dialog, _ -> dialog.dismiss() }
                .setPositiveButton("Yes", positiveButtonListener)
                .create()
                .show()
        }
    }
}