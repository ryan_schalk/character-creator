package com.ryanschalk.charactercreator.util

import java.lang.Exception

// Resource class borrowed [from Google](https://github.com/googlesamples/android-architecture-components/blob/master/GithubBrowserSample/app/src/main/java/com/android/example/github/vo/Resource.kt)

enum class ResourceStatus { SUCCESS, ERROR, LOADING }

data class Resource<out T>(val status: ResourceStatus, val data: T?, val exception: Exception?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(
                ResourceStatus.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(exception: Exception, data: T?): Resource<T> {
            return Resource(
                ResourceStatus.ERROR,
                data,
                exception
            )
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(
                ResourceStatus.LOADING,
                data,
                null
            )
        }
    }
}