package com.ryanschalk.charactercreator.util

import android.os.Bundle

interface SavedInstanceStateAware {
    fun onSaveInstanceState(outState: Bundle?)
    fun onRestoreInstanceState(savedInstanceState: Bundle?)
}