package com.ryanschalk.charactercreator.mainScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ryanschalk.charactercreator.R
import com.ryanschalk.charactercreator.createCharacter.navigation.CreatorActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createCharacterButton.setOnClickListener {
            startActivity(Intent(this, CreatorActivity::class.java))
        }
    }
}
