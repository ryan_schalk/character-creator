package com.ryanschalk.charactercreator

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import com.ryanschalk.charactercreator.createCharacter.chooseClass.ChooseClassAdapter
import com.ryanschalk.charactercreator.createCharacter.navigation.CreatorActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4


@RunWith(AndroidJUnit4::class)
class CreatorTests {
    @Rule
    @JvmField
    val rule = ActivityTestRule<CreatorActivity>(CreatorActivity::class.java)

    @Test
    fun test_correct_assign_text_shown_for_free_class() {
        ChooseClassRobot()
            .getClasses()
            .chooseFreeClassAtPosition(1)
            .verifyInstructionalText("Assign stats for your Bard:")
    }

    @Test
    fun test_correct_assign_text_shown_for_paid_class() {
        ChooseClassRobot()
            .getClasses()
            .choosePaidClassAtPosition(0)
            .confirmPurchase()
            .verifyInstructionalText("Assign stats for your Warrior:")
    }
}

class ChooseClassRobot {
    fun getClasses(): ChooseClassRobot {
        onView(withId(R.id.getClassesButton))
            .perform(click())

        Thread.sleep(2000)

        return this
    }

    fun chooseFreeClassAtPosition(position: Int): AssignStatsRobot {
        onView(withId(R.id.classRecyclerView))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<ChooseClassAdapter.ClassViewHolder>(position, click())
            )

        return AssignStatsRobot()
    }

    fun choosePaidClassAtPosition(position: Int): ChooseClassRobot {
        onView(withId(R.id.classRecyclerView))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<ChooseClassAdapter.ClassViewHolder>(position, click())
            )

        return this
    }

    fun confirmPurchase(): AssignStatsRobot {
        onView(withText("Yes")).perform(click())
        return AssignStatsRobot()
    }
}

class AssignStatsRobot {
    fun verifyInstructionalText(text: String): AssignStatsRobot {
        onView(withId(R.id.instructionalMessageTextView))
            .check(matches(withText(text)))

        return this
    }
}